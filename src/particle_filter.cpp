/*
 * particle_filter.cpp
 *
 *  Created on: Dec 12, 2016
 *      Author: Tiffany Huang
 */

#include <random>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <math.h> 
#include <iostream>
#include <sstream>
#include <string>
#include <iterator>
#include<vector>
#include "particle_filter.h"

using namespace std;

void ParticleFilter::init(double x, double y, double theta, double std[]) {
	// TODO: Set the number of particles. Initialize all particles to first position (based on estimates of 
	//   x, y, theta and their uncertainties from GPS) and all weights to 1. 
	// Add random Gaussian noise to each particle.
	// NOTE: Consult particle_filter.h for more information about this method (and others in this file).

	//initializingn random_engine class to generate pseudo random number
	default_random_engine generator;

	//number of particles
	num_particles = 101;


	//Normal Distribution for X,Y,Theta
	double std_x = std[0];
	double std_y = std[1];
	double std_theta = std[2]; 
	normal_distribution<double> dist_x(x, std_x);
	normal_distribution<double> dist_y(y,std_y);	
	normal_distribution<double> dist_theta(theta,std_theta);
	for(int i=0;i<num_particles;i++)
	{
		Particle p;
		p.id = i;
		p.x = dist_x(generator);
		p.y = dist_y(generator);
		p.theta = dist_theta(generator);
		p.weight = 1.0;

		particles.push_back(p);
	}
	for(auto i:particles)
	{
		cout<<i.weight<<" ";
	}
is_initialized = true;
}

 void ParticleFilter::prediction(double delta_t, double std_pos[], double velocity, double yaw_rate) {
	// TODO: Add measurements to each particle and add random Gaussian noise.
	// NOTE: When adding noise you may find std::normal_distribution and std::default_random_engine useful.
	//  http://en.cppreference.com/w/cpp/numeric/random/normal_distribution
	//  http://www.cplusplus.com/reference/random/default_random_engine/
	// cout<<"Begin Prediction"<<endl;
	default_random_engine generator;
	double std_x = std_pos[0];
	double std_y = std_pos[1];
	double std_theta = std_pos[2];
	for(int i=0;i<num_particles;i++)
	{
		double current_x,current_y,current_theta;
		current_x = particles[i].x;
		current_y = particles[i].y;
		current_theta = particles[i].theta;
		double new_x,new_y,new_theta;

		if(fabs(yaw_rate) > 0.00001){
		new_x = current_x+(velocity/yaw_rate)*(sin(current_theta+yaw_rate*delta_t)-sin(current_theta));
		new_y = current_y+(velocity/yaw_rate)*(cos(current_theta)-cos(current_theta+yaw_rate*delta_t));
		new_theta = current_theta+yaw_rate*delta_t;
		}
		else{
		// cout<<"in here"<<endl;
		new_x = current_x +velocity*cos(current_theta)*delta_t;
		new_y = current_y + velocity*sin(current_theta)*delta_t;
		new_theta = current_theta;
		}
		normal_distribution<double> dist_x(new_x, std_x);
		normal_distribution<double> dist_y(new_y,std_y);	
		normal_distribution<double> dist_theta(new_theta,std_theta);
		new_x = dist_x(generator);
		new_y = dist_y(generator);
		new_theta = dist_theta(generator);
		particles[i].x = new_x;
		particles[i].y = new_y;
		particles[i].theta = new_theta;
	
	}

}
std::vector<LandmarkObs> ParticleFilter::dataConversion(Particle p,std::vector<LandmarkObs> observations){
	// cout<<"data Conversion"<<endl;
	std::vector<LandmarkObs> result;
	for(int i=0;i<observations.size();i++)
	{
		double x  = p.x + cos(p.theta)*observations[i].x - (sin(p.theta)*observations[i].y);
		double y = p.y + sin(p.theta)*observations[i].x + (cos(p.theta)*observations[i].y);
		LandmarkObs temp;
		temp.x = x;
		temp.y = y;
		result.push_back(temp);
	}
	return result;
}

void ParticleFilter::dataAssociation(std::vector<LandmarkObs> predicted, std::vector<LandmarkObs>& observations,double sensor_range) {
	// TODO: Find the predicted measurement that is closest to each observed measurement and assign the 
	//   observed measurement to this particular landmark.
	// NOTE: this method will NOT be called by the grading code. But you will probably find it useful to 
	//   implement this method and use it as a helper during the updateWeights phase.
	// cout<<"data association"<<endl;
	for(int i=0;i<observations.size();i++)
	{
		double max_d = numeric_limits<double>::max();

		int min_index = 0;
		for(int j=0;j<predicted.size();j++)
		{
			double temp_distance = dist(observations[i].x,observations[i].y,predicted[j].x,predicted[j].y);
			if(temp_distance<max_d){
				max_d = temp_distance;
				min_index = j;
			}
		}
		observations[i].id = min_index;

	}


}

void ParticleFilter::updateWeights(double sensor_range, double std_landmark[], 
		const std::vector<LandmarkObs> &observations, const Map &map_landmarks) {
	// cout<<"Update Weight"<<endl;
	// TODO: Update the weights of each particle using a mult-variate Gaussian distribution. You can read
	//   more about this distribution here: https://en.wikipedia.org/wiki/Multivariate_normal_distribution
	// NOTE: The observations are given in the VEHICLE'S coordinate system. Your particles are located
	//   according to the MAP'S coordinate system. You will need to transform between the two systems.
	//   Keep in mind that this transformation requires both rotation AND translation (but no scaling).
	//   The following is a good resource for the theory:
	//   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
	//   and the following is a good resource for the actual equation to implement (look at equation 
	//   3.33
	//   http://planning.cs.uiuc.edu/node99.html

	std::vector<LandmarkObs>map_land;
	
	for(int i=0;i<num_particles;i++){
		std::vector<LandmarkObs> observations_converted = move(dataConversion(particles[i],observations));
		vector<LandmarkObs> temp_land ;
		for(int k=0;k<map_landmarks.landmark_list.size();k++)
		{
			if(fabs(map_landmarks.landmark_list[k].x_f - particles[i].x)<sensor_range && fabs(map_landmarks.landmark_list[k].y_f - particles[i].y) <sensor_range)
			{
				temp_land.push_back({map_landmarks.landmark_list[k].id_i,map_landmarks.landmark_list[k].x_f,map_landmarks.landmark_list[k].y_f});
			}
		}
		map_land = move(temp_land);
		dataAssociation(map_land,observations_converted,sensor_range);
		particles[i].sense_x.clear();
		particles[i].sense_y.clear();
		particles[i].associations.clear();
		double updated_weight = 1.0;
		for(int j=0;j<observations_converted.size();j++)
		{
			
			particles[i].associations.push_back(map_land[observations_converted[j].id].id);
			particles[i].sense_x.push_back(observations_converted[j].x);
			particles[i].sense_y.push_back(observations_converted[j].y);
			double ob_x = observations_converted[j].x;
			double ob_y = observations_converted[j].y;
			double ld_x = map_land[observations_converted[j].id].x;
			double ld_y = map_land[observations_converted[j].id].y;
			double s_x = std_landmark[0];
      		double s_y = std_landmark[1];
			double weight_temp = ( 1/(2*M_PI*s_x*s_y)) * exp( -( pow(ld_x-ob_x,2)/(2*pow(s_x, 2)) + (pow(ld_y-ob_y,2)/(2*pow(s_y, 2))) ) );
			updated_weight*=weight_temp;
			
		}
		particles[i].weight=updated_weight;
	}



}

void ParticleFilter::resample() {
	// cout<<"resample"<<endl;
	// TODO: Resample particles with replacement with probability proportional to their weight. 
	// NOTE: You may find std::discrete_distribution helpful here.
	//   http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution
	// cout<<"Before Resampling"<<endl;
	vector<double> test_weights;
	for(int i=0;i<num_particles;i++)
	{
		test_weights.push_back(particles[i].weight);
	}
	default_random_engine generator;

	discrete_distribution<int> d(test_weights.begin(),test_weights.end());
	vector<int>test_index;
	for(int i=0;i<num_particles;i++)
	{
		test_index.push_back(d(generator));
	}
	vector<Particle> new_particles ;
	for(int i=0;i<num_particles;i++)
	{
		new_particles.push_back(particles[test_index[i]]);
	}
	particles =move(new_particles);
}

Particle ParticleFilter::SetAssociations(Particle& particle, const std::vector<int>& associations, 
                                     const std::vector<double>& sense_x, const std::vector<double>& sense_y)
{
    //particle: the particle to assign each listed association, and association's (x,y) world  mapping to
    // associations: The landmark id that goes along with each listed associationcoordinates
    // sense_x: the associations x mapping already converted to world coordinates
    // sense_y: the associations y mapping already converted to world coordinates

    particle.associations= associations;
    particle.sense_x = sense_x;
    particle.sense_y = sense_y;
}

string ParticleFilter::getAssociations(Particle best)
{
	vector<int> v = best.associations;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<int>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseX(Particle best)
{
	vector<double> v = best.sense_x;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseY(Particle best)
{
	vector<double> v = best.sense_y;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
